defmodule VisitedLinksRegistry.SharedCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      import VisitedLinksRegistry.SharedTestHelper
      alias VisitedLinksRegistry.TimeSeriesRepo, as: Repo
      require Logger

      def assert_status(result, status, extend_message \\ "") when is_atom(status) do
        assert elem(result, 0) == status, inspect(result: result, status: status, message: extend_message)
        result
      end

      def refute_status(result, status, extend_message \\ "") when is_atom(status) do
        refute elem(result, 0) == status, inspect(result: result, status: status, message: extend_message)
        result
      end

      # may be return {left, right}
      def assert_equal(left, right, extend_message \\ "") do
        assert left == right, inspect(left: left, right: right, message: extend_message)
      end

      def refute_equal(left, right, extend_message \\ "") do
        refute left == right, inspect(left: left, right: right, message: extend_message)
      end


      def assert_time_index(time_index) do
        assert time_index_valid?(time_index), inspect(time_index: time_index)
      end

      def time_index_valid?(time_index) do
        time_index =~ ~r/^\d+-\d+$/
      end

    end
  end

end
