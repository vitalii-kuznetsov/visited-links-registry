defmodule VisitedLinksRegistry.SharedTestHelper do
  use ExUnitProperties

  alias VisitedLinksRegistry.TimeSeriesRepo, as: Repo

  def valid_links() do
    [
      "https://ya.ru",
      "https://ya.ru?q=123",
      "funbox.ru",
      "user@funbox.ru",
      "http://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor",
      "stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor",
      "host",
      "//host",
      "www.ya.ru/path/to/resource.html",
      "http://www.ya.ru:6789",
      "127.0.0.1",
      "127.0.0.1:1010",
      "http://127.0.0.1:2020/questions/11828270/",
      "[2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d]",
      "[2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d]:3030",
      "http://[2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d]:4040",
      "https%3A%2F%2Felixir-lang.org"
    ]
  end

  def valid_hosts() do
    [
      "ya.ru",
      "ya.ru",
      "funbox.ru",
      "funbox.ru",
      "stackoverflow.com",
      "stackoverflow.com",
      "host",
      "host",
      "www.ya.ru",
      "www.ya.ru",
      "127.0.0.1",
      "127.0.0.1",
      "127.0.0.1",
      "2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d",
      "2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d",
      "2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d",
      "elixir-lang.org"
    ]
  end

  def valid_unique_hosts() do
    Enum.uniq(valid_hosts())
  end

  def invalid_links() do
    [
      "",
      "https://",
      "ftp://host",
      "www.ya.ru:8080",
      "ldap://[2001:db8::7]/c=GB?objectClass?one",
      "mailto:John.Doe@example.com",
      "sip:911@pbx.mycompany.com",
      "news:comp.infosystems.www.servers.unix",
      "data:text/plain;charset=iso-8859-7,%be%be%be",
      "tel:+1-816-555-1212",
      "telnet://192.0.2.16:80/",
      "@/relative/URI/with/absolute/path/to/resource.txt",
      "/resource.txt#frag01",
      "#frag01",
    ]
  end

  def invalid_millisecond_timestamp_for_repo_list() do
    [
      "",
      1.5,
      :aaa,
      {}, {"a"},
      [], [1, "2"],
      1..10,
      'aaaaa',
      "aaaaa",
      "1000000-",
      "-10",
      "asd-33333",
      "333-aa",
      Repo.max_value() + 1,
      -Repo.max_value() - 1,
      "#{Repo.max_value() + 1}-0",
      "1-#{Repo.max_value() + 1}",
      -1
    ]
  end

  def invalid_millisecond_timestamp_for_registry_list() do
    invalid_millisecond_timestamp_for_repo_list() ++ [
      "100-0",
      "10000-1"
    ]
  end

  def default_repo_time_unit(), do: Repo.time_unit()

  def endpoint_time_unit(), do: :second

  def get_int_time_from_insert_result(insert_result) do
    {:ok, time_index} = insert_result
    get_int_time_from_time_index(time_index)
  end

  def get_int_time_from_time_index(time_index) when is_integer(time_index), do: time_index
  def get_int_time_from_time_index(time_index) when is_binary(time_index) do
    time_index
    |> String.split("-")
    |> hd()
    |> String.to_integer()
  end
  def get_int_time_from_time_index(time_index), do: get_int_time_from_time_index(to_string(time_index))


  def string_valid(params \\ []), do: string(:printable, params) |> add_correct_string_length_filter(params)

  # In StreamData.string() :max_length, :min_length and :length not always match String.length() value.
  # Now support only :max_length and :min_length.
  defp add_correct_string_length_filter(string_stream, params) do
    min_length = max(Keyword.get(params, :min_length, 0), 0)
    max_length = max(Keyword.get(params, :max_length, :infinity), 0)

    if (max_length < min_length), do:
      raise ArgumentError, message: "max_length (#{max_length}) must be >= min_length (#{min_length})"

    StreamData.map(string_stream, fn it ->
      it
      |> increase_if_shorter(min_length)
      |> crop_if_longer(max_length)
    end)
  end

  defp increase_if_shorter(str, min_length) do
    length = String.length(str)

    cond do
      length >= min_length -> str
      true ->
        str = if (length == 0) do "123" else str end
        String.duplicate(str, div(min_length, String.length(str)) + 1)
    end
  end

  defp crop_if_longer(str, max_length) do
    cond do
      String.length(str) > max_length -> String.slice(str, 0, max_length)
      true -> str
    end
  end

end
