defmodule VisitedLinksRegistryTest do
  use VisitedLinksRegistry.SharedCase
  import ExUnitProperties

  alias VisitedLinksRegistry, as: Registry

  @series_name Application.fetch_env!(:visited_links_registry, :series_name)

  setup context do
    {:ok, _} = Repo.clear_all_values_in_series(@series_name)
    context
  end

  describe "insert_unique_hosts_from_links/2" do
    test "when valid links, should inserted unique hosts from links" do
      links = valid_links()
      hosts = valid_unique_hosts()

      {:ok, time_index} =
        Registry.insert_unique_hosts_from_links(links, @series_name)
        |> assert_status(:ok, inspect(links: links))

      assert_time_index(time_index)

      Repo.get_values_by_interval(@series_name, time_index, time_index)
      |> assert_equal({:ok, hosts}, inspect(from: time_index, to: time_index))
    end

    test "when invalid links, should return error" do
      test_items = [
        [],
        invalid_links()
      ]

      for links <- test_items do
        Registry.insert_unique_hosts_from_links(links, @series_name)
        |> refute_status(:ok, inspect(links: links))
      end
    end
  end

  describe "links_to_hosts/1" do
    test "when valid links, should return hosts" do
      links = valid_links()
      hosts = valid_hosts()

      Registry.links_to_hosts(links)
      |> assert_equal(hosts, inspect(links: links))
    end

    test "when invalid link, should no return host for it" do
      links = invalid_links()
      hosts = Registry.links_to_hosts(links)

      assert hosts != [] && Enum.all?(hosts, fn it -> it == nil or it == "" end),
             inspect(links: links, hosts: hosts)
    end
  end

  describe "get_unique_hosts_by_interval/3" do
    test "when hosts included in valid interval, should get unique them all" do
      hosts = valid_unique_hosts()

      time1 =
        Repo.insert_list_with_unique_strings(@series_name, hosts)
        |> get_int_time_from_insert_result()

      time2 =
        Repo.insert_list_with_unique_strings(@series_name, hosts, time1 + 10)
        |> get_int_time_from_insert_result()

      unit = default_repo_time_unit()

      test_items = [
        {time1, time1, hosts},
        {time2, time2, hosts},
        {time1, time2, hosts},
        {time2, time1, hosts},
      ]

      for {from, to, hosts} <- test_items do
        Registry.get_unique_hosts_by_interval(from, to, unit, @series_name)
        |> assert_equal({:ok, hosts}, inspect(from: from, to: to, unit: unit, hosts: hosts))
      end
    end
  end

  describe "get_int_valid_interval/2" do
    test "when valid interval and unit, should return {:ok, from, to} where from <= to" do
      time_generator = valid_millisecond_time_generator()

      to_string_if_need = fn value, is_need? ->
        if is_need? do to_string(value) else value end
      end

      for unit <- get_valid_unit_list() do
        check all from <- time_generator,
                  to <- time_generator,
                  is_need_convert_from <- StreamData.boolean(),
                  is_need_convert_to <- StreamData.boolean() do

          from =
            from
            |> System.convert_time_unit(:millisecond, unit)
            |> to_string_if_need.(is_need_convert_from)

          to =
            to
            |> System.convert_time_unit(:millisecond, unit)
            |> to_string_if_need.(is_need_convert_to)

          res = Registry.get_int_valid_interval(from, to, unit)
          assert_status(res, :ok, inspect(from: from, to: to, unit: unit))

          {:ok, time_from, time_to} = res
          assert is_integer(time_from) and is_integer(time_to), inspect(from: from, to: to, unit: unit)
          assert time_from <= time_to, inspect(from: from, to: to, unit: unit)
        end
      end
    end
  end

  describe "get_unique_hosts_by_interval/3 and get_int_valid_interval/2" do
    test "when invalid interval, should return {:error, :invalid_data}" do
      unit = :millisecond

      for {from, to} <- invalid_millisecond_interval_list() do
        Registry.get_int_valid_interval(from, to, unit)
        |> assert_equal({:error, :invalid_data}, inspect(from: from, to: to, unit: unit))

        Registry.get_unique_hosts_by_interval(from, to, unit, @series_name)
        |> assert_equal({:error, :invalid_data}, inspect(from: from, to: to, unit: unit))
      end
    end

    test "when invalid unit, should return {:error, :invalid_data}" do
      [from, to] = Enum.take(valid_millisecond_time_generator(), 2)

      for unit <- invalid_unit_list() do
        Registry.get_int_valid_interval(from, to, unit)
        |> assert_equal({:error, :invalid_data}, inspect(from: from, to: to, unit: unit))

        Registry.get_unique_hosts_by_interval(from, to, unit, @series_name)
        |> assert_equal({:error, :invalid_data}, inspect(from: from, to: to, unit: unit))
      end
    end
  end


  defp valid_millisecond_time_generator(), do: StreamData.integer(0..Repo.max_value())

  defp get_valid_unit_list() do
    Enum.take(StreamData.positive_integer(), 2) ++ [
      :second,
      :millisecond,
      :microsecond,
      :nanosecond,
      :native
    ]
  end

  defp invalid_millisecond_interval_list() do
    valid_min = 0
    valid_max = Repo.max_value()

    invalid_millisecond_timestamp_for_registry_list()
    |> Enum.map(fn it-> [{valid_min, it}, {it, valid_max}] end)
    |> List.flatten()
  end

  defp invalid_unit_list() do
    [
      :second1,
      ":millisecond",
      1.5
    ]
  end

end
