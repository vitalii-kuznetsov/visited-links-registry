defmodule VisitedLinksRegistry.SharedTestHelperTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  import VisitedLinksRegistry.SharedTestHelper

  test "string_valid/1 when input :min_length and (or) :max_length,
       string length should be in the inputted range" do
    list = [0] ++ Enum.take(StreamData.positive_integer(), 35)
    for length <- list do
      check_string_min_max_length(min_length: length)
      check_string_min_max_length(max_length: length)
      check_string_min_max_length(min_length: length, max_length: (length + :rand.uniform(100)))
    end
  end

  defp check_string_min_max_length(opts) do
    min = Keyword.get(opts, :min_length, -1)
    max = Keyword.get(opts, :max_length, -1)

    for str <- Enum.take(string_valid(opts), 20) do
      length = String.length(str)

      if (min >= 0) do assert min <= length end
      if (max >= 0) do assert length <= max end
    end
  end

end
