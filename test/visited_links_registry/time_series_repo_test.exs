defmodule VisitedLinksRegistry.TimeSeriesRepoTest do
  use VisitedLinksRegistry.SharedCase

  @series_name Application.fetch_env!(:visited_links_registry, :series_name)

  setup context do
    {:ok, _} = Repo.clear_all_values_in_series(@series_name)
    context
  end

  test "all methods support any binary for series_name" do
    values = generate_valid_unique_values()
    series_name_list = [""] ++ Enum.take(StreamData.binary(), 50)

    for series_name <- series_name_list do
      Repo.insert_list_with_unique_strings(series_name, values)
      |> assert_status(:ok, inspect(series_name: series_name, values: values))

      Repo.get_values_by_interval(series_name, "-", "+")
      |> assert_status(:ok, inspect(series_name: series_name, from: "-", to: "+"))

      Repo.get_unique_values_by_interval(series_name, "-", "+")
      |> assert_status(:ok, inspect(series_name: series_name, from: "-", to: "+"))

      Repo.clear_all_values_in_series(series_name)
      |> assert_status(:ok, inspect(series_name: series_name))
    end
  end

  describe "insert_list_with_unique_strings/3" do
    test "when input valid values,
          should insert unique values for current time and return {:ok, time-index}" do
      count_values_in_list = 20
      count_iterations = 4

      insert_valid_values_test([], count_iterations, count_values_in_list)
    end

    defp insert_valid_values_test(_all_values, counter, _count_values) when counter <= 0, do: nil
    defp insert_valid_values_test(all_values, counter, count_values_in_list) do
      values_with_duplicates = generate_valid_values_with_duplicates()
      unique_values = Enum.uniq(values_with_duplicates)

      all_values = all_values ++ unique_values

      {:ok, time_index} =
        Repo.insert_list_with_unique_strings(@series_name, values_with_duplicates)
        |> assert_status(:ok, inspect(values: values_with_duplicates))

      assert_time_index(time_index)

      Repo.get_values_by_interval(@series_name, "-", time_index)
      |> assert_equal({:ok, all_values}, inspect(from: "-", to: time_index))

      insert_valid_values_test(all_values, counter - 1, count_values_in_list)
    end

    test "when input valid values and timestamp,
          should insert unique values for timestamp and return {:ok, time-index}" do
      valid_timestamp_order_list = [
        1,
        10,
        100,
        "1000-0",
        '1001',
        "123456-#{Repo.max_value()}",
        "#{Repo.max_value()}-#{Repo.max_value()}"
      ]

      for timestamp <- valid_timestamp_order_list do
        values_with_duplicates = generate_valid_values_with_duplicates()
        unique_values = Enum.uniq(values_with_duplicates)

        {:ok, time_index} =
          Repo.insert_list_with_unique_strings(@series_name, values_with_duplicates, timestamp)
          |> assert_status(:ok, inspect(values: values_with_duplicates, timestamp: timestamp))

        time = get_int_time_from_time_index(time_index)
        timestamp = get_int_time_from_time_index(timestamp)

        assert_time_index(time_index)
        assert time == timestamp

        Repo.get_values_by_interval(@series_name, time, time)
        |> assert_equal({:ok, unique_values}, inspect(from: time, to: time))
      end
    end

    test "when invalid values, should no insert and return error" do
      valid_values = generate_valid_unique_values()

      invalid_values = [
        "",
        :aaa,
        {}, {"a"},
        [], [1, "2"],
        12,
        1.5,
        1..10
      ]

      refute_status(Repo.insert_list_with_unique_strings(@series_name, []), :ok, inspect(values: []))

      for invalid_value <- invalid_values do
        values = [invalid_value | valid_values]
        refute_status(Repo.insert_list_with_unique_strings(@series_name, values), :ok, inspect(values: values))
      end
    end

    test "when invalid timestamp, should no insert and return error" do
      values = generate_valid_unique_values()

      for timestamp <- invalid_millisecond_timestamp_for_repo_list() do
        Repo.insert_list_with_unique_strings(@series_name, values, timestamp)
        |> refute_status(:ok, inspect(values: values, timestamp: timestamp))
      end

      # new timestamp must be > last timestamp
      invalid_timestamp_order_list = [100, 10, 1]
      Repo.insert_list_with_unique_strings(@series_name, values, 100)

      for timestamp <- invalid_timestamp_order_list do
        Repo.insert_list_with_unique_strings(@series_name, values, timestamp)
        |> refute_status(:ok, inspect(values: values, timestamp: timestamp))
      end
    end
  end

  describe "get_values_by_interval/3" do
    test "when values included in valid interval, should get them all" do
      values1 = generate_valid_unique_values()
      values2 = generate_valid_unique_values()

      time1 =
        Repo.insert_list_with_unique_strings(@series_name, values1)
        |> get_int_time_from_insert_result()

      time2 =
        Repo.insert_list_with_unique_strings(@series_name, values2, time1 + 10)
        |> get_int_time_from_insert_result()

      all_values = values1 ++ values2

      test_items = [
        {time1, "#{time1}", values1},
        {"#{time2}-0", time2, values2},
        {time1, time2, all_values},
        {time2, time1, []},

        {0, Repo.max_value(), all_values},
        {0, "#{Repo.max_value()}-#{Repo.max_value()}", all_values},
        {"-", "+", all_values},

        {time1 - 1, time1 - 1, []},
        {time2 + 1, time2 + 1, []},
        {time1, time2 - 1, values1},
        {time1 + 1, time2, values2},
        {0, '0', []},
      ]

      for {from, to, values} <- test_items do
        Repo.get_values_by_interval(@series_name, from, to)
        |> assert_equal({:ok, values}, inspect(from: from, to: to))
      end
    end
  end

  describe "get_unique_values_by_interval/3" do
    test "when values included in valid interval, should get unique them all" do
      values = generate_valid_unique_values()

      time1 =
        Repo.insert_list_with_unique_strings(@series_name, values)
        |> get_int_time_from_insert_result()

      time2 =
        Repo.insert_list_with_unique_strings(@series_name, values, time1 + 10)
        |> get_int_time_from_insert_result()

      test_items = [
        {time1, "#{time1}", values},
        {"#{time2}-0", time2, values},
        {time1, time2, values},
        {time2, time1, []},

        {0, Repo.max_value(), values},
        {0, "#{Repo.max_value()}-#{Repo.max_value()}", values},
        {"-", "+", values},

        {time1 - 1, time1 - 1, []},
        {time2 + 1, time2 + 1, []},
        {time1, time2 - 1, values},
        {time1 + 1, time2, values},
        {0, '0', []},
      ]

      for {from, to, values} <- test_items do
        Repo.get_unique_values_by_interval(@series_name, from, to)
        |> assert_equal({:ok, values}, inspect(from: from, to: to))
      end
    end
  end

  describe "get_values_by_interval/3 and get_unique_values_by_interval/3" do
    test "when invalid interval, should get error" do
      invalid_interval_list =
        invalid_millisecond_timestamp_for_repo_list()
        |> Enum.map(fn it-> [{"-", it}, {it, "+"}] end)
        |> List.flatten()

      for {from, to} <- invalid_interval_list do
        Repo.get_values_by_interval(@series_name, from, to)
        |> refute_status(:ok, inspect(from: from, to: to))

        Repo.get_unique_values_by_interval(@series_name, from, to)
        |> refute_status(:ok, inspect(from: from, to: to))
      end
    end
  end

  describe "clear_all_values_in_series/1" do
    test "when series included values, should remove all values" do
      valid_values = generate_valid_unique_values()

      assert_status(Repo.clear_all_values_in_series(@series_name), :ok)

      Repo.insert_list_with_unique_strings(@series_name, valid_values)

      Repo.get_values_by_interval(@series_name, "-", "+")
      |> assert_equal({:ok, valid_values}, inspect(from: "-", to: "+"))

      assert_status(Repo.clear_all_values_in_series(@series_name), :ok)

      Repo.get_values_by_interval(@series_name, "-", "+")
      |> assert_equal({:ok, []}, inspect(from: "-", to: "+"))
    end
  end


  defp generate_valid_values_with_duplicates() do
    unique_values = generate_valid_unique_values()
    duplicates =
      unique_values
      |> hd()
      |> List.duplicate(4)

    duplicates ++ unique_values
  end

  defp generate_valid_unique_values() do
    string_valid(min_length: 1)
    |> Enum.take(20)
    |> Enum.uniq()
  end

end
