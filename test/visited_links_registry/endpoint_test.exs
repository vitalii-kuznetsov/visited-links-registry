defmodule VisitedLinksRegistry.EndpointTest do
  use VisitedLinksRegistry.SharedCase
  use Plug.Test

  alias VisitedLinksRegistry.Endpoint

  @series_name Application.fetch_env!(:visited_links_registry, :series_name)

  @opts Endpoint.init([])
  @ok_status "ok"
  @invalid_data_status "error: invalid input data"

  setup context do
    {:ok, _} = Repo.clear_all_values_in_series(@series_name)
    context
  end

  describe "post /visited_links" do
    test "when send valid links and other parameters,
          should insert only links and return status ok,
          other parameters ​​is ignored" do
      links = valid_links()
      hosts = valid_unique_hosts()

      other_link = "http://other.com"
      refute Enum.member?(links, other_link), inspect(links: links, other_link: other_link)

      send_json =
        %{"links" => links, "other1" => 10, "other2" => %{"links" => [other_link]}}
        |> Jason.encode!()

      conn =
        conn(:post, "/visited_links", send_json)
        |> put_req_header("content-type", "application/json")
        |> Endpoint.call(@opts)

      assert conn.state == :sent, inspect(send_json: send_json, conn: conn)
      assert conn.status == 200, inspect(send_json: send_json, conn: conn)

      decode_resp_body(conn)
      |> assert_json_status(@ok_status, inspect(send_json: send_json, conn: conn))

      Repo.get_values_by_interval(@series_name, "-", "+")
      |> assert_equal({:ok, hosts}, inspect(from: "-", to: "+", send_json: send_json, conn: conn))
    end

    test "when send invalid links, should return #{@invalid_data_status}" do
      links = invalid_links()
      send_json = Jason.encode!(%{"links" => links})

      conn =
        conn(:post, "/visited_links", send_json)
        |> put_req_header("content-type", "application/json")
        |> Endpoint.call(@opts)

      assert conn.state == :sent, inspect(send_json: send_json, conn: conn)
      assert conn.status == 200, inspect(send_json: send_json, conn: conn)

      decode_resp_body(conn)
      |> assert_json_status(@invalid_data_status, inspect(send_json: send_json, conn: conn))
    end

    test "when not send links, should return status #{@invalid_data_status}" do
      conn =
        conn(:post, "/visited_links")
        |> Endpoint.call(@opts)

      assert conn.state == :sent, inspect(conn: conn)
      assert conn.status == 200, inspect(conn: conn)
      assert_json_status(decode_resp_body(conn), @invalid_data_status, inspect(conn: conn))
    end
  end

  describe "get /visited_domains" do
    setup do
      hosts = valid_unique_hosts()

      # when time is converted it is rounded
      time =
        Repo.insert_list_with_unique_strings(@series_name, hosts)
        |> get_int_time_from_insert_result()
        |> System.convert_time_unit(default_repo_time_unit(), endpoint_time_unit())

      %{hosts: hosts, time: time}
    end

    test "when send valid interval and other parameters,
          should return hosts included in interval and status ok,
          other parameters ​​is ignored", context do
      hosts = context.hosts
      time = context.time

      test_items = [
        {time, time + 1, hosts},
        {time - 2, time - 1, []}
      ]

      for {from, to, hosts} <- test_items do
        conn =
          conn(:get, "/visited_domains?other=10&from=#{from}&to=#{to}")
          |> Endpoint.call(@opts)

        resp_body = decode_resp_body(conn)

        assert conn.state == :sent, inspect(conn: conn)
        assert conn.status == 200, inspect(conn: conn)
        assert_json_status(resp_body, @ok_status, inspect(conn: conn))
        assert_json_value(resp_body, "domains", hosts, inspect(conn: conn))
      end
    end

    test "when one or two parameters of interval not send or invalid,
          should return #{@invalid_data_status}", context do
      time = context.time

      from = time
      to = time + 1

      query_params_list = [
        "",
        "?from=#{from}",
        "?to=#{to}",
        "?from1=#{from}&to=#{to}",
        "?from=#{from}&to1=#{to}",
      ]

      for query_params <- query_params_list do
        conn =
          conn(:get, "/visited_domains" <> query_params)
          |> Endpoint.call(@opts)

        resp_body = decode_resp_body(conn)

        assert conn.state == :sent, inspect(conn: conn)
        assert conn.status == 200, inspect(conn: conn)
        assert_json_status(resp_body, @invalid_data_status, inspect(conn: conn))
      end
    end
  end

  defp decode_resp_body(conn), do: Jason.decode!(conn.resp_body)

  defp assert_json_status(resp_body, status, extend_message) do
    assert_json_value(resp_body, "status", status, extend_message)
  end

  defp assert_json_value(resp_body, key, value, extend_message) do
    assert Map.get(resp_body, key) == value,
           inspect(resp_body: resp_body, key: key, value: value, message: extend_message)
  end

end
