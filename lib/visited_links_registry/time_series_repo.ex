defmodule VisitedLinksRegistry.TimeSeriesRepo do
  @moduledoc """
  Simple implementation time series repository based on redis streams.
  For work with redis using eredis client.

  For more information, see:

    * redis streams - https://redis.io/topics/streams-intro
    * eredis client - https://github.com/wooga/eredis
  """
  require Logger

  @conn :time_series_eredis_conn
  @empty_value ""

  @doc """
  Time unit in redis streams is `:millisecond`.
  """
  def time_unit(), do: :millisecond

  @doc """
  Max value for `<millisecondsTime>` and `<sequenceNumber>`. See more in `insert_list_with_unique_strings/3`.
  """
  def max_value(), do: 18_446_744_073_709_551_615

  @doc """
  Inserts a list with unique not empty strings.
  If list include duplicates they will be removed.

  ## Parameters

    * `series_name`: string that represents the key name of stream in redis.
    * `values`: list of values for inserted. Each value must be `binary()` and no empty. List must be no empty.
    * `timestamp`: timestamp which will be associated values. Timestamp must be >= last timestamp saved in series.
    If timestamp not inputted timestamp wil be set in redis. Usually redis set current time but there are exceptions.

  Timestamp must be `integer()` or `binary()` in format: `<millisecondsTime>-<sequenceNumber>` or `<millisecondsTime>`.
  Example: `1586454174024-0` or `1586454174024`.
    * `<millisecondsTime>`: associate timestamp. In this method range values is [1, `max_value()/0`].
    * `<sequenceNumber>`: is sequence number for equal `<millisecondsTime>`. Range values is [0, `max_value()/0`].

  ## Return values

    * `{:ok, timestamp}`, when `timestamp` is `binary()` in `<millisecondsTime>-<sequenceNumber>` format.
    * `{:error, reason}`.
  """
  def insert_list_with_unique_strings(series_name, values, timestamp \\ "*")
      when is_binary(series_name) and is_list(values) do
    case valid_values?(values) && !is_float(timestamp) do
      true ->
        values = optimized_values_for_insert_to_stream(values)
        :eredis.q(@conn, ["XADD", series_name, timestamp] ++ values)

      false ->
        {:error, :invalid_data}
    end
  end

  defp valid_values?(values) do
    values != [] && Enum.all?(values, fn it -> is_binary(it) && it != "" end)
  end

  defp optimized_values_for_insert_to_stream(values) when is_list(values) do
    values = Enum.uniq(values)
    cond do
      rem(length(values), 2) != 0 -> [@empty_value | values]
      true -> values
    end
  end

  defp get_stream_items_by_interval(series_name, time_from, time_to) when is_binary(series_name) do
    case interval_valid?(time_from, time_to) do
      true -> :eredis.q(@conn, ["XRANGE", series_name, time_from, time_to])
      false -> {:error, :invalid_data}
    end
  end

  defp interval_valid?(time_from, time_to) do
    !is_float(time_from) && !is_float(time_to)
  end

  defp get_converted_items_by_interval(series_name, time_from, time_to, convert_items_fun)
      when is_binary(series_name) and is_function(convert_items_fun) do
    case get_stream_items_by_interval(series_name, time_from, time_to) do
      {:ok, items} -> {:ok, convert_items_fun.(items)}
      result -> result
    end
  end

  @doc """
  Returns list with all values included in interval is [time_from, time_to].

  ## Parameters

    * `series_name`: description see in `insert_list_with_unique_strings/3`
    * `time_from`, `time_to` - timestamps interval values. Description see in `insert_list_with_unique_strings/3`.
    But for this method range values of timestamps part `<millisecondsTime>` are different and is
    [0, `max_value()/0`], i.e. 0 is supported in here.

  ## Return values

    * `{:ok, values}`
    * `{:error, reason}`

  For return correct values must be `time_from` <= `time_to`.
  If `time_from` > `time_to` will be returned empty list.
  """
  def get_values_by_interval(series_name, time_from, time_to) when is_binary(series_name) do
    get_converted_items_by_interval(
      series_name,
      time_from, time_to,
      &convert_stream_items_to_values/1
    )
  end

  @doc """
  Similar to the function `get_values_by_interval/3` but returns only unique values.
  """
  def get_unique_values_by_interval(series_name, time_from, time_to) when is_binary(series_name) do
    get_converted_items_by_interval(
      series_name,
      time_from, time_to,
      &convert_stream_items_to_unique_values/1
    )
  end

  defp convert_stream_items_to_values(items) when is_list(items) do
    items
    |> remove_keys_and_optimized_values()
    |> List.flatten()
  end

  defp convert_stream_items_to_unique_values(items) when is_list(items) do
    items
    |> convert_stream_items_to_values()
    |> Enum.uniq()
  end

  defp remove_keys_and_optimized_values(items) when is_list(items) do
    Enum.map(items, fn it ->
      values_list = hd(tl(it))

      cond do
        hd(values_list) == @empty_value -> tl(values_list)
        true -> values_list
      end
    end)
  end

  @doc """
  Clear all values in series.

  Just deletes the series_name key in redis.
  After that, functions `get_unique_values_by_interval/3` and `get_values_by_interval/3` returns empty list.

  ## Parameters

    * `series_name`: description see in `insert_list_with_unique_strings/3`

  ## Return values

    * `{:ok, binary()}`
    * `{:error, reason}`
  """
  def clear_all_values_in_series(series_name) when is_binary(series_name) do
    :eredis.q(@conn, ["DEL", series_name])
  end


  @doc false
  def child_spec(_opts) do
    {:ok, config} = Application.fetch_env(:visited_links_registry, __MODULE__)

    Logger.info("Time series repo config: #{inspect(config)}")

    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [config]}
    }
  end

  @doc false
  def start_link(opts \\ []) do
    result = :eredis.start_link(opts)

    case result do
      {:ok, pid} -> Process.register(pid, @conn)
    end

    result
  end
end
