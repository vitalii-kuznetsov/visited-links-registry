defmodule VisitedLinksRegistry.Endpoint do
  @moduledoc false

  use Plug.Router
  alias VisitedLinksRegistry, as: Registry
  require Logger

  plug :match

  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason

  plug :dispatch

  post "/visited_links" do
    links = Map.get(conn.body_params, "links", nil)

    response = try do
      case Registry.insert_unique_hosts_from_links(links) do
        {:ok, _} -> response_with_status(:ok)
        error -> response_error(error)
      end
    rescue
      exception -> response_exception_error(exception)
    end

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, response)
  end

  get "/visited_domains" do
    time_from = Map.get(conn.query_params, "from", nil)
    time_to = Map.get(conn.query_params, "to", nil)

    response = try do
      case Registry.get_unique_hosts_by_interval(time_from, time_to, :second) do
        {:ok, hosts} -> response_with_status(:ok, %{domains: hosts})
        error -> response_error(error)
      end
    rescue
      exception -> response_exception_error(exception)
    end

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, response)
  end


  defp response_exception_error(_exception), do: response_with_status("error: was raised exception error")

  defp response_error({:error, :invalid_data}), do: response_with_status("error: invalid input data")
  defp response_error({error, reason}), do: response_with_status("#{error}: #{reason}")

  defp response_with_status(status, %{} = map \\ %{}) do
    map
    |> Map.put(:status, status)
    |> Jason.encode!()
  end


  def child_spec(_opts) do
    {:ok, config} = Application.fetch_env(:visited_links_registry, __MODULE__)
    config = Keyword.put(config, :plug, __MODULE__)

    Logger.info("Endpoint address: http://localhost:#{config[:options][:port]}/")

    # may be to change ID to __MODULE__
    Plug.Cowboy.child_spec(config)
  end

end
