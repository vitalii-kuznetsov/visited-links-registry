defmodule VisitedLinksRegistry do
  @moduledoc """
  """
  alias VisitedLinksRegistry.TimeSeriesRepo, as: Repo

  @series_name Application.fetch_env!(:visited_links_registry, :series_name)

  @doc """
  Converts links to hosts and saved only unique hosts.

  Support only links with full valid URL with http(s) schemes or without scheme.
  This means that:
    * valid links for example: `http://ya.ru`, `//ya.ru` and `ya.ru`.
    * but `ya.ru:8080` is invalid link, because "www.ya.ru" interpreted how URI scheme which != http(s).
    If need to insert `ya.ru:8080` must be use such formats `http://ya.ru:8080` or `//ya.ru:8080`

  ## Parameters
    * `links`: list of links. Each link must be `binary()` and no empty. List must be no empty.
    * `series_name`: description see in `VisitedLinksRegistry.TimeSeriesRepo.insert_list_with_unique_strings/3`.
    By default uses series_name inputted in configs.

  ## Return values

    * `{:ok, timestamp}`, same as in `VisitedLinksRegistry.TimeSeriesRepo.insert_list_with_unique_strings/3`.
    * `{:error, reason}`.
  """
  def insert_unique_hosts_from_links(links, series_name \\ @series_name) do
    hosts = links_to_hosts(links)

    case valid_hosts?(hosts) do
      true -> Repo.insert_list_with_unique_strings(series_name, hosts)
      false -> {:error, :invalid_data}
    end
  end

  defp valid_hosts?(hosts) do
    # host == "" when Uri.parse("http://")
    hosts != nil &&
      hosts != [] &&
      !Enum.any?(hosts, fn it -> it == nil or it == "" end)
  end

  @doc false
  def links_to_hosts(links) when is_list(links) do
    links
    |> Enum.map(&link_to_host/1)
  end

  def links_to_hosts(_), do: nil

  defp link_to_host(link) when is_binary(link) and link != "" do
    uri =
      link
      |> URI.decode()
      |> URI.parse()

    host = uri.host
    scheme = uri.scheme

    cond do
      !is_nil(scheme) && scheme != "http" && scheme != "https" -> nil
      is_nil(host) && is_nil(scheme) -> URI.parse("http://" <> link).host
      true -> host
    end
  end

  defp link_to_host(_), do: nil

  @doc """
  Returns list with unique hosts included in interval is [time_from, time_to].
  If `time_from` > `time_to` they are swapped and interval becomes to [time_to, time_from].

  ## Parameters

    * `time_from`, `time_to`: timestamps interval values. Must be integer() or binary() in `unit` format.
    Example: `1586454174024` for unit `:millisecond`.
    * `unit` - time unit of `time_from` and `time_to` values.
    Supported all values when support `System.convert_time_unit/2`.
    * `series_name`: description see in `insert_unique_hosts_from_links/2`.

  `time_from` and `time_to` values will be converted to time unit of repository,
  see `VisitedLinksRegistry.TimeSeriesRepo.time_unit()/0`.

  ## Return values

    * `{:ok, hosts}`
    * `{:error, reason}`
  """
  def get_unique_hosts_by_interval(time_from, time_to, unit, series_name \\ @series_name)
      when is_binary(series_name) do
    case get_int_valid_interval(time_from, time_to, unit) do
      {:ok, from, to} -> Repo.get_unique_values_by_interval(series_name, from, to)
      result -> result
    end
  end

  # @spec get_valid_time(integer | binary, integer | binary,:native | System.time_unit()) :: integer
  @doc false
  def get_int_valid_interval(time_from, time_to, unit) do
    try do
      from = to_int_correct_unit_time(time_from, unit)
      to = to_int_correct_unit_time(time_to, unit)

      cond do
        time_valid?(from) && time_valid?(to) ->
          {:ok, min(from, to), max(from, to)}

        true ->
          {:error, :invalid_data}
      end
    rescue
      _ -> {:error, :invalid_data}
    end
  end

  defp to_int_correct_unit_time(time, unit) when is_binary(time),
    do: to_int_correct_unit_time(String.to_integer(time), unit)

  defp to_int_correct_unit_time(time, unit) when is_integer(time),
    do: System.convert_time_unit(time, unit, Repo.time_unit())

  defp time_valid?(time), do: 0 <= time and time <= Repo.max_value()

end
