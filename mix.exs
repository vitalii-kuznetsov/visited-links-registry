defmodule VisitedLinksRegistry.MixProject do
  use Mix.Project

  def project do
    [
      app: :visited_links_registry,
      version: "0.1.0",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {VisitedLinksRegistry.Application, []}
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.1"},
      {:eredis, "~> 1.2"},
      {:stream_data, "~> 0.4.3", only: :test},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
    ]
  end
end
