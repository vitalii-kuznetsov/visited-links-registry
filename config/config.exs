import Config

config :visited_links_registry, VisitedLinksRegistry.Endpoint,
  scheme: :http,
  options: [port: 4000]

config :visited_links_registry, VisitedLinksRegistry.TimeSeriesRepo,
  host: '192.168.204.128',
  port: 6379,
  database: 0,
  password: ''
  # reconnect_sleep: 100,
  # connect_timeout: 5000,
  # socket_options: []

config :visited_links_registry,
  series_name: "test:time-series:visited-links-registry"

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

import_config "#{Mix.env()}.exs"
