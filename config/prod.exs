use Mix.Config

config :visited_links_registry, VisitedLinksRegistry.Endpoint,
  scheme: :http,
  options: [port: 80]

config :visited_links_registry, VisitedLinksRegistry.TimeSeriesRepo,
  host: '192.168.204.128',
  port: 6379,
  database: 1,
  password: ''

config :visited_links_registry,
  series_name: "time-series:visited-links-registry"
