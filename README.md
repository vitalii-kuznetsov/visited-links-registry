# VisitedLinksRegistry

In app uses web server `cowboy`: https://ninenines.eu/docs/en/cowboy/2.7/guide/  
For work with `cowboy` uses `Plug` (https://hexdocs.pm/plug/) and adapter `Plug.Cowboy` (https://hexdocs.pm/plug_cowboy/).  

App saves only unique hosts of links. For storage hosts and timestamps uses `redis streams`: https://redis.io/topics/streams-intro   
For work with redis uses `eredis` client: https://github.com/wooga/eredis  

For more information see in documentation of modules 
[VisitedLinksRegistry](https://bitbucket.org/vitalii-kuznetsov/visited-links-registry/src/master/lib/visited_links_registry.ex) 
and [VisitedLinksRegistry.TimeSeriesRepo](https://bitbucket.org/vitalii-kuznetsov/visited-links-registry/src/master/lib/visited_links_registry/time_series_repo.ex). 
You can generate documentation with ExDoc. About it next.

# How to start
## 1.	Install and run additional software
* `elixir ~>1.9` and `mix`
* `Redis 5+`. My version is `5.0.8`. For started redis server I am using command `/redis-server --port 6379 --protected-mode no`. Other settings by default.
## 2.	Clone repository
## 3.	Get and compile project dependencies
Run commands in terminal:

* `cd "~project_dir/"`
* `mix deps.get`
* `mix deps.compile`
## 4.	Correct project configurations
For it correct files in `~project_dir/config/`. Now settings contain only files:

* `~project_dir/config/config.exs`: default configurations.
* `~project_dir/config/prod.exs`: for productions.

Only 3 blocks are important.
### 4.1. First block - endpoint configure
```elixir
config :visited_links_registry, VisitedLinksRegistry.Endpoint,
  scheme: :http,
  options: [port: 4000]
```
Here you can configure Plug.Cowboy for start cowboy server with VisitedLinksRegistry.Endpoint.  
Parameters:

* `:scheme` - either :http or :https
* `:options` - keyword list of Plug.Cowboy options. You can use all options. Description options see: https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html#module-options 

Example includes minimal settings.
### 4.2. Second block - TimeSeriesRepo configure
```elixir
config :visited_links_registry, VisitedLinksRegistry.TimeSeriesRepo,
  host: '192.168.204.128',
  port: 6379,
  database: 0,
  password: ''
  # reconnect_sleep: 100,
  # connect_timeout: 5000,
  # socket_options: []
```

TimeSeriesRepo work based on eredis client. Here need configure eredis options for connection to redis server.  
Supported all eredis parameters (source: https://github.com/wooga/eredis/#commands):

* `:host` - dns name or ip address as **charlist**; or unix domain socket as {local, Path} (available in OTP 19+). Default is '127.0.0.1'.
* `:port` - integer. Default is 6379.
* `:database` - integer or 0 for default database. Default is 0.
* `:password` - **charlist** or empty **charlist** for no password. Default is ''.
* `:reconnect_sleep` - integer of milliseconds to sleep between reconnect attempts. Default is 100.
* `:connect_timeout` - timeout value in milliseconds to use in gen_tcp:connect. Default is 5000.
* `:socket_options` - proplist of options to be sent to gen_tcp:connect. Default is [].

You can enter any parameters or nothing.
### 4.3. Third block - series_name configure
```elixir
config :visited_links_registry,
  series_name: "test:time-series:visited-links-registry"
```
`:series_name` - is key name of stream object in redis which using for storage data and timestamps. Support any `binary()` values.

In the example indicated test value of series_name.
## 5.	Using
Now you can run commands in termimal:

* `mix run --no-halt` for run app.
* `MIX_ENV=test` and `mix test` for testing. Tests work only with `MIX_ENV=test` because some deps and modules compile only for `test` environment.
* `MIX_ENV=dev` and `mix docs` for generate documentation of some modules with ExDoc. Find docs in `~project_dir/doc/`